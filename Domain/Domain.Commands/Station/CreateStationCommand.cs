﻿namespace Domain.Commands.Station
{
    using Infrastructure.Domain.Commands;
    using Infrastructure.EntityFramework;
    using Model.Station;
    using Web.Application.Station.CommandContexts;

    public class CreateStationCommand : ICommand<CreateStationCommandContext>
    {
        private readonly IRepository<Station> _repository;

        public CreateStationCommand(IRepository<Station> repository)
        {
            _repository = repository;
        }

        public void Execute(CreateStationCommandContext commandContext)
        {
            var form = commandContext.Form;

            var station = new Station
            {
                TimeReference = form.Name
            };

            _repository.Add(station);
        }
    }
}