﻿namespace Domain.Model.Sensor
{
    using Infrastructure.Domain;
    public class SensorName : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}