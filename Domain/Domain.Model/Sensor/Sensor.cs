﻿namespace Domain.Model.Sensor
{
    using Infrastructure.Domain;
    using Station;

    public class Sensor : IEntity
    {
        public int Id { get; set; }

        public Station Station { get; set; }

        public SensorName SensorName { get; set; }

        public string Number { get; set; }

        public string SerialNumber { get; set; }

        public string Comment { get; set; }
    }
}