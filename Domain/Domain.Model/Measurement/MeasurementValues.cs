﻿namespace Domain.Model.Measurement
{
    using System;
    using Infrastructure.Domain;

    public class MeasurementValues : IEntity
    {
        public int Id { get; set; }

        public int MeasurementId { get; set; }

        public int ParameterId { get; set; }

        public string Value { get; set; }

        public DateTime MeasurementDate { get; set; }
    }
}