﻿namespace Domain.Model.Measurement
{
    using System;
    using Infrastructure.Domain;

    public class Measurement : IEntity
    {
        public int Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ReportingInterval { get; set; }

        public string InstrumentHeight { get; set; }

        public string Comment { get; set; }

        public string ParametersCount { get; set; }

    }
}