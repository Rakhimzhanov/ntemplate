﻿namespace Domain.Model.Station
{
    using Infrastructure.Domain;

    public class StationDescription : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}