﻿namespace Domain.Model.Station
{
    using Infrastructure.Domain;

    public class Station : IEntity
    {
        public int Id { get; set; }

        public StationDescription StationDescription { get; set; }

        public StationType StationType { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string TimeReference { get; set; }
    }
}