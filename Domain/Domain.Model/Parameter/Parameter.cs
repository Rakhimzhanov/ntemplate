﻿namespace Domain.Model.Parameter
{
    using Infrastructure.Domain;
    using Sensor;

    public class Parameter : IEntity
    {
        public int Id { get; set; }

        public Sensor Sensor { get; set; }

        public ParameterType ParameterType { get; set; }

        public ParameterGroup ParameterGroup { get; set; }

        public ParameterUnit ParameterUnit { get; set; }

        public string Height { get; set; }

        public string Average { get; set; }
    }
}