﻿namespace Domain.Model.Parameter
{
    using Infrastructure.Domain;
    public class ParameterGroup : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}