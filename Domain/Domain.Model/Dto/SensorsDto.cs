﻿namespace Domain.Model.Dto
{
    public class SensorsDto
    {
        public string ParameterSensor { get; set; }

        public string ParameterSensorNumber { get; set; }

        public string SerialNumber { get; set; }

        public string Comment { get; set; }

        public string ParameterGroup { get; set; }

        public string ParameterHeight { get; set; }

        public string ParameterType { get; set; }

        public string ParameterUnits { get; set; }

        public string ParameterAverage { get; set; }
    }
}