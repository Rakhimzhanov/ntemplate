﻿namespace Domain.Model.Dto
{
    public class HeadersDto
    {

        public string StationDescription { get; set; }

        public string StationType { get; set; }

        public string StationLatitude { get; set; }

        public string StationLongitude { get; set; }

        public string TimeReference { get; set; }

        public string MeasurementStart { get; set; }

        public string MeasurementEnd { get; set; }

        public string ReportingInterval { get; set; }

        public string InstrumentHeight { get; set; }

        public string Comment { get; set; }

        public string NumberOfParameters { get; set; }
    }
}