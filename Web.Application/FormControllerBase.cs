﻿namespace Web.Application
{
    using System;
    using System.Net;
    using System.Web.Mvc;
    using Castle.Core.Logging;
    using Castle.Windsor;
    using Infrastrcuture.Web.Forms;
    using Infrastructure.Domain.Exceptions;
    using Infrastructure.EntityFramework;
    using Services.Account;
    using Services.Account.Models;

    public class FormControllerBase : Controller, ICurrentUserAccessor
    {
        public JsonResult Form<TForm>(TForm form)
            where TForm : IForm
        {
            var formHanlderFactory = ResolveFormHandlerFactory();
            var unitOfWork = ResolveUnitOfWork();
            var logger = ResolveLogger();

            try
            {
                logger.Info($"Begin request of <{CurrentUser.DisplayNameWithNk}> with form <{ form.GetType().Name }>.");

                formHanlderFactory.Create<TForm>().Execute(form);

                unitOfWork.SaveChanges();

                logger.Info($"Complete request of <{CurrentUser.DisplayNameWithNk}> with form <{ form.GetType().Name }>.");

                return Json(new { form });
            }
            catch (BusinessException be)
            {
                return JsonError(form, be, logger);
            }
            catch (FormHandlerException fhe)
            {
                return JsonError(form, fhe, logger);
            }
            catch (Exception e)
            {
                return JsonError(form, e, logger);
            }
        }

        //Add exception logging
        public FileResult FileForm<TForm>(TForm form)
            where TForm : IFileForm
        {
            var formHanlderFactory = ResolveFormHandlerFactory();

            formHanlderFactory.Create<TForm>().Execute(form);

            return File(form.FileContent, System.Net.Mime.MediaTypeNames.Application.Octet, form.FileName);

        }

        private JsonResult JsonError<TForm>(TForm form, Exception e, ILogger logger)
        {
            logger.Error($"Rollback request of <{CurrentUser.DisplayNameWithNk}> with form <{ form.GetType().Name }>.", e);

            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return Json(new
            {
                form,
                exceptionMessage = e.Message
            });
        }

        #region Dependency resolution

        private IFormHandlerFactory ResolveFormHandlerFactory()
        {
            return GetContainer().Resolve<IFormHandlerFactory>();
        }

        private IUnitOfWork ResolveUnitOfWork()
        {
            return GetContainer().Resolve<IUnitOfWork>();
        }

        private ILogger ResolveLogger()
        {
            return GetContainer().Resolve<ILogger>();
        }

        private IWindsorContainer GetContainer()
        {
            var containerAccessor = HttpContext.ApplicationInstance as IContainerAccessor;
            return containerAccessor.Container;
        }

        private ICurrentUserKeeper ResolveCurrentUserKeeper()
        {
            return GetContainer().Resolve<ICurrentUserKeeper>();
        }

        #endregion

        #region CurrentUserAccessor Memebers

        public ApplicationUser CurrentUser
        {
            get
            {
                var currentUserKeeper = ResolveCurrentUserKeeper();
                return currentUserKeeper.GetCurrentUser();
            }
        }

        #endregion
    }
}