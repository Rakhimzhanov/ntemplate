﻿namespace Web.Application.Services.Account
{
    public interface IAppConfig
    {
        string TestLogonName { get; }
    }
}