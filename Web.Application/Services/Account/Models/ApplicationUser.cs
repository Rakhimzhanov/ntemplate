﻿namespace Web.Application.Services.Account.Models
{
    using Infrastructure.Web.Helpers;

    public class ApplicationUser
    {
        public string DisplayName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string LoginName { get; set; }

        public string Position { get; set; }

        public string Department { get; set; }

        public string Division { get; set; }

        public string LocationCode { get; set; }

        public string LineManagerEmail { get; set; }

        public string HeadEmail { get; set; }

        public string DepartmentManagerEmail { get; set; }
        
        public string Nk => NkHelper.FromLoginName(LoginName);

        public string DisplayNameWithNk => DisplayName + "  (NK" + Nk + ")";
    }
}