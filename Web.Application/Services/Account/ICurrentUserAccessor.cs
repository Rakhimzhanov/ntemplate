﻿namespace Web.Application.Services.Account
{
    using Models;

    public interface ICurrentUserAccessor
    {
        ApplicationUser CurrentUser { get; }
    }
}