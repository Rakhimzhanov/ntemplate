﻿namespace Web.Application.Services.Account.Impl
{
    using System.Web;
    using Infrastructure.Web.Helpers;
    using Models;

    public class CurrentUserKeeper : ICurrentUserKeeper
    {
        private ApplicationUser _currentUser;
        private readonly IAppConfig _config;

        public CurrentUserKeeper(ApplicationUser currentUser, IAppConfig config)
        {
            _currentUser = currentUser;
            _config = config;
        }

        public ApplicationUser GetCurrentUser()
        {
            if (_currentUser == null)
            {
                AppSession.CurrentUser = null;
                _currentUser = AppSession.CurrentUser;

                if (_currentUser == null)
                {
                    var loginName = _config.TestLogonName;

                    if (string.IsNullOrWhiteSpace(loginName))
                    {
                        loginName = HttpContext.Current.User.Identity.Name;
                    }

                    _currentUser = LoadUser(loginName);

                    AppSession.CurrentUser = _currentUser;
                }
            }

            return _currentUser;
        }

        private ApplicationUser LoadUser(string loginName)
        {
            var nk = NkHelper.FromLoginName(loginName);
            return new ApplicationUser
            {
                FullName = nk
            };
        }
    }
}