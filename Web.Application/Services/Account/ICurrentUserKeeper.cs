﻿namespace Web.Application.Services.Account
{
    using Models;

    public interface ICurrentUserKeeper
    {
        ApplicationUser GetCurrentUser();
    }
}