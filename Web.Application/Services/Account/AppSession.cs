﻿namespace Web.Application.Services.Account
{
    using System.Web;
    using Models;

    public class AppSession
    {
        public static ApplicationUser CurrentUser
        {
            get
            {
                var value = (ApplicationUser)HttpContext.Current.Session["__ApplicationUser__"];

                return value;
            }
            set
            {
                HttpContext.Current.Session["__ApplicationUser__"] = value;
            }
        }
    }
}