﻿namespace Web.Application
{
    using System;
    using System.Net;
    using System.Web.Mvc;
    using Castle.Windsor;
    using Infrastrcuture.Web.Forms;
    using Infrastructure.Domain.Exceptions;
    using Infrastructure.EntityFramework;

    public class JsonFormControllerBase : Controller
    {
        public JsonResult Form<TForm>(TForm form)
            where TForm : IForm
        {
            var formHandlerFactory = ResolveFormHandlerFactory();
            var unitOfWork = ResolveUnitOfWork();

            try
            {
                formHandlerFactory.Create<TForm>().Execute(form);

                unitOfWork.SaveChanges();

                return Json(new {form});

            }
            catch (BusinessException be)
            {
                return JsonError(new
                {
                    form,
                    exceptionMessage = be.Message
                });
            }
            catch (FormHandlerException fhe)
            {
                return JsonError(new
                {
                    form,
                    exceptionMessage = fhe.Message
                });
            }
        }

        private JsonResult JsonError(object result)
        {
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return Json(result);
        }

        #region Dependency resolution

        private IFormHandlerFactory ResolveFormHandlerFactory()
        {
            return GetContainer().Resolve<IFormHandlerFactory>();
        }

        private IUnitOfWork ResolveUnitOfWork()
        {
            return GetContainer().Resolve<IUnitOfWork>();
        }

        private IWindsorContainer GetContainer()
        {
            var containerAccessor = HttpContext.ApplicationInstance as IContainerAccessor;
            return containerAccessor.Container;
        }

        #endregion
    }
}