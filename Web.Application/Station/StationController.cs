﻿namespace Web.Application.Station
{
    using System.Web.Mvc;

    public class StationController : FormControllerBase
    {
        [HttpGet]
        public ActionResult List()
        {
            return View();
        }
    }
}