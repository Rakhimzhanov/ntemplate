﻿namespace Web.Application.Station
{
    using Domain.Model.Station;
    using Infrastructure.EntityFramework;

    public class StationsController : ReadODataControllerBase<Station>
    {
        public StationsController(IRepository<Station> repository) : base(repository)
        {
        }
    }
}