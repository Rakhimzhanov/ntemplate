﻿namespace Web.Application.Station.CommandContexts
{
    using Forms;
    using Infrastructure.Domain.Commands;
    public class CreateStationCommandContext : ICommandContext
    {
         public CreateStationForm Form { get; set; }
    }
}