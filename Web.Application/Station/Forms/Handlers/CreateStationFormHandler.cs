﻿namespace Web.Application.Station.Forms.Handlers
{
    using CommandContexts;
    using Infrastrcuture.Web.Forms;
    using Infrastructure.Domain.Commands;

    public class CreateStationFormHandler : IFormHandler<CreateStationForm>
    {
        private readonly ICommandBuilder _commandBuilder;

        public CreateStationFormHandler(ICommandBuilder commandBuilder)
        {
            _commandBuilder = commandBuilder;
        }

        public void Execute(CreateStationForm form)
        {
            _commandBuilder.Execute(new CreateStationCommandContext
            {
                Form = form
            });
        }
    }
}