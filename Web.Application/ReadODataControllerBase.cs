﻿namespace Web.Application
{
    using System.Linq;
    using System.Web.Http.OData;
    using Infrastructure.Domain;
    using Infrastructure.EntityFramework;

    public class ReadODataControllerBase<TEntity> : ODataController
        where TEntity : class, IEntity
    {
        private readonly IRepository<TEntity> _repository;

        public ReadODataControllerBase(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public IQueryable<TEntity> Get()
        {
            return _repository.Query();
        }
    }
}