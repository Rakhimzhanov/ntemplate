﻿namespace Web.Helpers
{
    using System.Text;
    using System.Web.Mvc;

    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// An Html helper for Require.js
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="common">Location of the common js file.</param>
        /// <param name="module">Location of the main.js file.</param>
        /// <returns></returns>
        public static MvcHtmlString RequireJs(this HtmlHelper helper, string common, string module)
        {
            var require = new StringBuilder();

            var jsLocation = "Scripts/";

            require.AppendLine("<script type=\"text/javascript\">");
            require.AppendLine($@"require( [ ""/{jsLocation}{common}.js"" ], function() {{");
            require.AppendLine($"    require( [ \"/{jsLocation}{module}.js\"] );");
            require.AppendLine("});");
            require.AppendLine("</script>");

            return new MvcHtmlString(require.ToString());
        }
    }
}