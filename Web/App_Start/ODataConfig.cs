﻿namespace Web
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using Domain.Model.Station;
    using Infrastrcuture.Web.Extensions;
    using Microsoft.Data.Edm;

    public class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var builder = new ODataConventionModelBuilder();

            config.Routes.MapODataServiceRoute("odata", "odata", GetEdmModel(builder));
        }

        public static IEdmModel GetEdmModel(ODataConventionModelBuilder builder)
        {
            var entityTypes = typeof (Station).Assembly.GetTypes().Where(x => x.IsClass && !x.IsNested);

            var method = builder.GetType().GetMethod("EntitySet");

            foreach (var entityType in entityTypes)
            {
                var genericMethod = method.MakeGenericMethod(entityType);

                genericMethod.Invoke(builder, new object[]
                {
                    entityType.Name.Pluralize()
                });
            }

            return builder.GetEdmModel();
        }
    }
}