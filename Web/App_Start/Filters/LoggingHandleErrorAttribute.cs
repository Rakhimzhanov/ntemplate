﻿namespace Web.Filters
{
    using System.Web.Mvc;
    using Infrastructure.Logging.Log4Net;

    public class LoggingHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            var logger = new Log4NetLogger();
            logger.Error($"Exception in {filterContext.HttpContext.Request.Url}", filterContext.Exception);
        }
    }
}