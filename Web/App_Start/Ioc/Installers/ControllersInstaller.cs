﻿namespace Web.Ioc.Installers
{
    using System.Web.Http.OData;
    using System.Web.Mvc;
    using Application.Station;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<StationController>()
                .BasedOn<IController>()
                .LifestyleTransient());

            container.Register(Classes.FromAssemblyContaining<StationsController>()
                .BasedOn<ODataController>()
                .LifestyleTransient());
        }
    }
}