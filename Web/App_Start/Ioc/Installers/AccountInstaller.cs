﻿namespace Web.Ioc.Installers
{
    using Application.Services.Account;
    using Application.Services.Account.Impl;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    public class AccountInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ICurrentUserKeeper>().ImplementedBy<CurrentUserKeeper>().LifestylePerWebRequest());
        }
    }
}