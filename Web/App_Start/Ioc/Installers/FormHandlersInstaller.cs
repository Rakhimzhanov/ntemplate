﻿namespace Web.Ioc.Installers
{
    using Application.Station.CommandContexts;
    using Application.Station.Forms.Handlers;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Infrastrcuture.Web.Forms;

    public class FormHandlersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var formHandlers = Types.FromAssemblyContaining<CreateStationFormHandler>()
                .BasedOn(typeof (IFormHandler<>))
                .WithService.AllInterfaces()
                .LifestyleTransient();

        }
    }
}