﻿namespace Web.Ioc.Installers
{
    using Castle.Facilities.TypedFactory;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Domain.Commands.Station;
    using Infrastructure.Domain.Commands;

    public class CommandInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<TypedFactoryFacility>();

            var commands = Types.FromAssemblyContaining<CreateStationCommand>()
                .BasedOn(typeof (ICommand<>))
                .WithService.AllInterfaces()
                .LifestyleTransient();

            container.Register(commands,
                Component.For<ICommandBuilder>().ImplementedBy<CommandBuilder>().LifestyleTransient(),
                Component.For<ICommandFactory>().AsFactory().LifestyleTransient());
        }
    }
}