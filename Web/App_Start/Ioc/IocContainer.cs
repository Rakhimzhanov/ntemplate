﻿namespace Web.Ioc
{
    using System.Web.Http;
    using System.Web.Http.Dispatcher;
    using System.Web.Mvc;
    using Castle.Windsor;
    using Castle.Windsor.Installer;

    public class IocContainer
    {
        private static IWindsorContainer _container;

        public static IWindsorContainer Setup()
        {
            _container = new WindsorContainer().Install(FromAssembly.This());

            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            GlobalConfiguration.Configuration.Services.Replace(
                    typeof(IHttpControllerActivator),
                    new WindsorCompositionRoot(_container)
                    );

            return _container;
        }
    }
}