﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/libs/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/libs/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/_libs/kendo/2014.3.1119/kendo.web.min.js",
                "~/Scripts/_libs/kendo/jszip.min.js"
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/libs/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/_libs/bootstrap.js",
                      "~/Scripts/_libs/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/css/font-awesome.min.css",
                    "~/Content/css/datepicker.css",
                    "~/Content/css/panel-table.css",
                    "~/Content/css/Gridmvc.css",
                    "~/Content/css/datatables.min.css",
                    "~/Content/css/jquery-ui-bootstrap-autocomplete.css",
                    "~/Content/libs/kendo/2014.2.716/kendo.common.min.css",
                    "~/Content/libs/kendo/custom/kendo.custom.bootstrap-lightgray.css",
                    "~/Content/css/bootstrap.min.css",
                    "~/Content/css/kendo-custom.css",
                    "~/Content/css/notification.css",
                    "~/Content/css/navbar-fixed-side.css",
                    "~/Content/css/helpers.css",
                    "~/Content/css/site.css",
                    "~/Content/css/navbar-fixed-side.css",
                    "~/Content/css/datatables.min.css"));
        }
    }
}
