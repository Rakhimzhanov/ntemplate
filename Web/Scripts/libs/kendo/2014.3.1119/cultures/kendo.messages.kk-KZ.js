﻿define(['kendo'], function(kendo) {

/* Filter menu operator messages */

    if (kendo.ui.FilterMenu) {
        kendo.ui.FilterMenu.prototype.options.operators =
            $.extend(true, kendo.ui.FilterMenu.prototype.options.operators, {
                "date": {
                    "eq": "тең",
                    "gte": "кейін не тең",
                    "gt": "кейін",
                    "lte": "дейін не тең",
                    "lt": "дейін",
                    "neq": "тең емес"
                },
                "number": {
                    "eq": "тең",
                    "gte": "көп не тең",
                    "gt": "көп",
                    "lte": "аз не тең",
                    "lt": "аз",
                    "neq": "тең емес"
                },
                "string": {
                    "endswith": "бітеді",
                    "eq": "тең",
                    "neq": "тең емес",
                    "startswith": "басталады",
                    "contains": "ішінде кездеседі",
                    "doesnotcontain": "ішінде кездеспейді"
                },
                "enum": {
                    "eq": "тең",
                    "neq": "тең емес"
                }
            });
    }

/* ColumnMenu messages */

    if (kendo.ui.ColumnMenu) {
        kendo.ui.ColumnMenu.prototype.options.messages =
            $.extend(true, kendo.ui.ColumnMenu.prototype.options.messages, {
                "columns": "Бағандар",
                "sortAscending": "Өсу реті бойынша сұрыптау",
                "sortDescending": "Кему реті бойынша сұрыптау",
                "settings": "Баған параметрлері",
                "done": "Жасалды",
                "lock": "Жабу",
                "unlock": "Ашу",
                "filter": "Сүзгілеу"
            });
    }

/* RecurrenceEditor messages */

    if (kendo.ui.RecurrenceEditor) {
        kendo.ui.RecurrenceEditor.prototype.options.messages =
            $.extend(true, kendo.ui.RecurrenceEditor.prototype.options.messages, {
                "daily": {
                    "interval": "days(s)",
                    "repeatEvery": "Repeat every:"
                },
                "end": {
                    "after": "After",
                    "occurrence": "occurrence(s)",
                    "label": "End:",
                    "never": "Never",
                    "on": "On",
                    "mobileLabel": "Ends"
                },
                "frequencies": {
                    "daily": "Daily",
                    "monthly": "Monthly",
                    "never": "Never",
                    "weekly": "Weekly",
                    "yearly": "Yearly"
                },
                "monthly": {
                    "day": "Day",
                    "interval": "month(s)",
                    "repeatEvery": "Repeat every:",
                    "repeatOn": "Repeat on:"
                },
                "offsetPositions": {
                    "first": "first",
                    "fourth": "fourth",
                    "last": "last",
                    "second": "second",
                    "third": "third"
                },
                "weekly": {
                    "repeatEvery": "Repeat every:",
                    "repeatOn": "Repeat on:",
                    "interval": "week(s)"
                },
                "yearly": {
                    "of": "of",
                    "repeatEvery": "Repeat every:",
                    "repeatOn": "Repeat on:",
                    "interval": "year(s)"
                },
                "weekdays": {
                    "day": "day",
                    "weekday": "weekday",
                    "weekend": "weekend day"
                }
            });
    }

/* Grid messages */

    if (kendo.ui.Grid) {
        kendo.ui.Grid.prototype.options.messages =
            $.extend(true, kendo.ui.Grid.prototype.options.messages, {
                "commands": {
                    "create": "Қосу",
                    "destroy": "Жою",
                    "canceledit": "Болдыртпау",
                    "update": "Жаңарту",
                    "edit": "Өзгерту",
                    "select": "Таңдау",
                    "cancel": "Өзгерістерді болдыртпау",
                    "save": "Өзгерістерді сақтау"
                },
                "editable": {
                    "confirmation": "Шынымен бұл жазуды жойғыңыз келе ме?",
                    "cancelDelete": "Болдыртпау",
                    "confirmDelete": "Жою"
                }
            });
    }

/* Pager messages */

    if (kendo.ui.Pager) {
        kendo.ui.Pager.prototype.options.messages =
            $.extend(true, kendo.ui.Pager.prototype.options.messages, {
                "page": "Бет",
                "display": "{2} ішінен {0} - {1} жазу көрсетілген",
                "of": " барлығы - {0}",
                "empty": "Көрсетілетін жазулар жоқ",
                "refresh": "Жаңарту",
                "first": "Біренші бетке оралу",
                "itemsPerPage": "беттегі жазулар саны",
                "last": "Соңғы бетке көшу",
                "next": "Келесі бетке көшу",
                "previous": "Алдыңғы бетке оралу",
                "morePages": "Бет санын көбейту"
            });
    }

/* FilterMenu messages */

    if (kendo.ui.FilterMenu) {
        kendo.ui.FilterMenu.prototype.options.messages =
            $.extend(true, kendo.ui.FilterMenu.prototype.options.messages, {
                "filter": "сүзгілеу",
                "and": "Және",
                "clear": "Тазарту",
                "info": "Мәні",
                "selectValue": "-таңдаңыз-",
                "isFalse": "жалған",
                "isTrue": "рас",
                "or": "әйтпесе",
                "cancel": "Болдыртпау",
                "operator": "Оператор",
                "value": "Мәні"
            });
    }

/* Groupable messages */

    if (kendo.ui.Groupable) {
        kendo.ui.Groupable.prototype.options.messages =
            $.extend(true, kendo.ui.Groupable.prototype.options.messages, {
                "empty": "Переместите сюда заголовок колонки, чтобы сгрупировать записи из этой колонки"
            });
    }

/* Editor messages */

    if (kendo.ui.Editor) {
        kendo.ui.Editor.prototype.options.messages =
            $.extend(true, kendo.ui.Editor.prototype.options.messages, {
                "bold": "Полужирный",
                "createLink": "Вставить гиперссылку",
                "fontName": "Шрифт",
                "fontNameInherit": "(шрифт как в документе)",
                "fontSize": "Выбрать размер шрифта",
                "fontSizeInherit": "(размер как в документе)",
                "formatBlock": "Текст изображения",
                "indent": "Увеличить отступ",
                "insertHtml": "Вставить HTML",
                "insertImage": "Изображение",
                "insertOrderedList": "Нумерованный список",
                "insertUnorderedList": "Маркированныйсписок",
                "italic": "Курсив",
                "justifyCenter": "По центру",
                "justifyFull": "По ширине",
                "justifyLeft": "Влево",
                "justifyRight": "Вправо",
                "outdent": "Уменьшить отступ",
                "strikethrough": "Зачеркнутый",
                "styles": "Стиль",
                "subscript": "Под строкой",
                "superscript": "Над строкой",
                "underline": "Подчеркнутый",
                "unlink": "Удалить гиперссылку",
                "dialogButtonSeparator": "или",
                "dialogCancel": "Отмена",
                "dialogInsert": "Вставить",
                "imageAltText": "Alternate text",
                "imageWebAddress": "Веб адрес",
                "linkOpenInNewWindow": "Открыть в новом окне",
                "linkText": "Текст",
                "linkToolTip": "Всплывающая подсказка",
                "linkWebAddress": "Веб адрес",
                "search": "Поиск",
                "createTable": "Вставить таблицу",
                "addColumnLeft": "Add column on the left",
                "addColumnRight": "Add column on the right",
                "addRowAbove": "Add row above",
                "addRowBelow": "Add row below",
                "deleteColumn": "Delete column",
                "deleteRow": "Delete row",
                "backColor": "Background color",
                "deleteFile": "Are you sure you want to delete \"{0}\"?",
                "directoryNotFound": "A directory with this name was not found.",
                "dropFilesHere": "drop files here to upload",
                "emptyFolder": "Empty Folder",
                "foreColor": "Color",
                "invalidFileType": "The selected file \"{0}\" is not valid. Supported file types are {1}.",
                "orderBy": "Arrange by:",
                "orderByName": "Name",
                "orderBySize": "Size",
                "overwriteFile": "'A file with name \"{0}\" already exists in the current directory. Do you want to overwrite it?",
                "uploadFile": "Upload",
                "formatting": "Format",
                "viewHtml": "View HTML",
                "dialogUpdate": "Update",
                "insertFile": "Insert file"
            });
    }

/* Upload messages */

    if (kendo.ui.Upload) {
        kendo.ui.Upload.prototype.options.localization =
            $.extend(true, kendo.ui.Upload.prototype.options.localization, {
                "cancel": "Отменить загрузку",
                "dropFilesHere": "перетащите сюда файлы для загрузки",
                "remove": "Удалить",
                "retry": "Повторить",
                "select": "Выбрать...",
                "statusFailed": "загрузка прервана",
                "statusUploaded": "загружено",
                "statusUploading": "загружается",
                "uploadSelectedFiles": "Загрузить выбранные файлы",
                "headerStatusUploaded": "Done",
                "headerStatusUploading": "Uploading..."
            });
    }

/* Scheduler messages */

    if (kendo.ui.Scheduler) {
        kendo.ui.Scheduler.prototype.options.messages =
            $.extend(true, kendo.ui.Scheduler.prototype.options.messages, {
                "allDay": "all day",
                "cancel": "Отмена",
                "confirmation": "Are you sure you want to delete this event?",
                "date": "Date",
                "destroy": "Delete",
                "editor": {
                    "allDayEvent": "All day event",
                    "description": "Description",
                    "editorTitle": "Event",
                    "end": "End",
                    "endTimezone": "End timezone",
                    "repeat": "Repeat",
                    "separateTimezones": "Use separate start and end time zones",
                    "start": "Start",
                    "startTimezone": "Start timezone",
                    "timezone": " ",
                    "timezoneEditorButton": "Time zone",
                    "timezoneEditorTitle": "Timezones",
                    "title": "Title",
                    "noTimezone": "No timezone"
                },
                "event": "Event",
                "recurrenceMessages": {
                    "deleteRecurring": "Do you want to delete only this event occurrence or the whole series?",
                    "deleteWindowOccurrence": "Delete current occurrence",
                    "deleteWindowSeries": "Delete the series",
                    "deleteWindowTitle": "Delete Recurring Item",
                    "editRecurring": "Do you want to edit only this event occurrence or the whole series?",
                    "editWindowOccurrence": "Edit current occurrence",
                    "editWindowSeries": "Edit the series",
                    "editWindowTitle": "Edit Recurring Item"
                },
                "save": "Save",
                "time": "Time",
                "today": "Today",
                "views": {
                    "agenda": "Agenda",
                    "day": "Day",
                    "month": "Month",
                    "week": "Week",
                    "workWeek": "Work Week"
                },
                "deleteWindowTitle": "Delete event",
                "showFullDay": "Show full day",
                "showWorkDay": "Show business hours"
            });
    }

/* Validator messages */

    if (kendo.ui.Validator) {
        kendo.ui.Validator.prototype.options.messages =
            $.extend(true, kendo.ui.Validator.prototype.options.messages, {
                "required": "{0} толтырылуы қажет",
                "pattern": "{0} қате",
                "min": "{0} {1} - ден артық не тең болуы қажет ",
                "max": "{0} {1} - ден кем не тең болуы қажет",
                "step": "{0} қате",
                "email": "{0} қате email",
                "url": "{0} қате URL",
                "date": "{0} қате дата"
            });
    }
});