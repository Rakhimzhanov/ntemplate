﻿namespace Web.Configs
{
    using System.Configuration;
    using Application.Services.Account;
    public class AppConfig : IAppConfig
    {
        public string TestLogonName => ConfigurationManager.AppSettings["TestLogonName"];
    }
}