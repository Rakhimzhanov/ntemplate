﻿namespace Infrastructure.EntityFramework
{
    using System.Linq;
    using Domain;

    public interface IRepository<TEntity>
        where TEntity : class , IEntity
    {
        TEntity GetById(int id);

        IQueryable<TEntity> Query();

        void Add(TEntity entity);

        void Delete(TEntity entity);
    }
}