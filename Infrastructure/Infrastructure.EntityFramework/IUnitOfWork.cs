﻿namespace Infrastructure.EntityFramework
{
    using System.Data.Entity;
    using Domain;

    public interface IUnitOfWork
    {
        void SaveChanges();

        IDbSet<TEntity> Set<TEntity>() where TEntity : class, IEntity;
    }
}