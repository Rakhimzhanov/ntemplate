﻿namespace Infrastructure.EntityFramework
{
    using System.Data.Entity;
    using Conventions;
    using Domain;
    using global::Domain.Model.Station;

    public class MetOceanModel : DbContext, IUnitOfWork
    {
        public MetOceanModel() : base("name=Main")
        {
        }

        public virtual DbSet<Station> Stations { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new ColumnNameUpperCaseConvention());
            modelBuilder.Types().Configure(x => x.ToTable(NameConventions.GetTableName(x.ClrType)));
        }

        public void SaveChanges()
        {
            SaveChanges();
        }

        public new IDbSet<TEntity> Set<TEntity>()
            where TEntity : class, IEntity
        {
            return base.Set<TEntity>();
        } 
    }
}