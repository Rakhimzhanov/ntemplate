﻿namespace Infrastructure.EntityFramework
{
    public interface ITransaction
    {
        void BeginTransaction();

        void Commit();

        void Rollback();
    }
}