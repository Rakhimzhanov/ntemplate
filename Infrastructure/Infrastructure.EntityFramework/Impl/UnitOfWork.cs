﻿namespace Infrastructure.EntityFramework.Impl
{
    using System.Data.Entity;
    using Domain;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public IDbSet<TEntity> Set<TEntity>() where TEntity : class, IEntity
        {
            return _context.Set<TEntity>();
        }
    }
}