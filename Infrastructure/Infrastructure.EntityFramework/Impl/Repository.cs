﻿namespace Infrastructure.EntityFramework.Impl
{
    using System.Linq;
    using Domain;

    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class , IEntity
    {
        private readonly IUnitOfWork _uow;

        public Repository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public TEntity GetById(int id)
        {
            return _uow.Set<TEntity>().FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<TEntity> Query()
        {
            return _uow.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _uow.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _uow.Set<TEntity>().Remove(entity);
        }
    }
}