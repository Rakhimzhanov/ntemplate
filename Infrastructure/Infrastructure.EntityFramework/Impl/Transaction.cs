﻿namespace Infrastructure.EntityFramework.Impl
{
    using System.Data.Entity;

    public class Transaction : ITransaction
    {
        private readonly DbContext _context;

        public Transaction(DbContext context)
        {
            _context = context;
        }

        public void BeginTransaction()
        {
            _context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _context.Database.CurrentTransaction.Commit();
        }

        public void Rollback()
        {
            _context.Database.CurrentTransaction.Rollback();
        }
    }
}