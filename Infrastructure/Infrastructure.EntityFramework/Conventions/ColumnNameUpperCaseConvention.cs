﻿namespace Infrastructure.EntityFramework.Conventions
{
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.ModelConfiguration.Conventions;
    public class ColumnNameUpperCaseConvention : IStoreModelConvention<EdmProperty>
    {
        public void Apply(EdmProperty item, DbModel model)
        {
            if (item.Name == "Id")
            {
                item.Name = NameConventions.GetPrimaryKeyColumnName(item.DeclaringType.Name);
            }
            else
            {
                item.Name = NameConventions.ReplaceCamelCaseWithUnderscore(item.Name).ToUpper();
            }
        }
    }
}