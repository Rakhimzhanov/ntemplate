﻿namespace Infrastructure.EntityFramework.Conventions
{
    using System;
    using System.Text.RegularExpressions;

    public static class NameConventions
    {
        public static string GetTableName(Type type)
        {
            return ReplaceCamelCaseWithUnderscore(type.Name);
        }

        internal static string ReplaceCamelCaseWithUnderscore(string name)
        {
            return Regex.Replace(name, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1_").ToUpper();
        }

        public static string GetPrimaryKeyColumnName(string typeName)
        {
            return string.Format("{0}_ID", ReplaceCamelCaseWithUnderscore(typeName));
        }
    }
}