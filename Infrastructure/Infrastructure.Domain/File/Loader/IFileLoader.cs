﻿namespace Infrastructure.Domain.File.Loader
{
    public interface IFileLoader
    {
        void LoadFile(string fileName);
    }
}