﻿namespace Infrastructure.Domain.File.Copy
{
    public interface IFileCopier
    {
        void Copy(string pathFrom, string pathTo);
    }
}