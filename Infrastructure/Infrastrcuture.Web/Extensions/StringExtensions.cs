﻿namespace Infrastrcuture.Web.Extensions
{
    using System.Data.Entity.Design.PluralizationServices;
    using System.Globalization;

    public static class StringExtensions
    {
        public static string Pluralize(this string s)
        {
            return PluralizationService.CreateService(CultureInfo.CurrentCulture).Pluralize(s);
        }
        public static string Singularize(this string s)
        {
            return PluralizationService.CreateService(CultureInfo.CurrentCulture).Singularize(s);
        }
    }
}