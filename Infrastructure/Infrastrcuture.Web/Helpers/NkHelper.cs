﻿namespace Infrastructure.Web.Helpers
{
    public class NkHelper
    {
        public static string FromLoginName(string loginName)
        {
            return loginName.Substring(loginName.Length - 8, 8);
        }

        public static string ToLoginName(string nk)
        {
            return "NK" + nk;
        }
    }
}