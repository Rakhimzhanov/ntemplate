﻿namespace Infrastructure.WinForms.Forms
{
    public interface IFileForm : IForm
    {
        string FileName { get; set; }

        byte[] FileContent { get; set; }
    }
}