﻿namespace Infrastructure.Logging.Log4Net
{
    using System;
    using log4net;

    public class Log4NetLogger : ILogger
    {
        private const string StrLogAppenderName = "Log4NetLogger";

        private readonly ILog _logger = LogManager.GetLogger(StrLogAppenderName);

        public void Debug(string message)
        {
            Reconfig();
            _logger?.Debug(message);
        }

        public void Info(string message)
        {
            Reconfig();
            _logger.Info(message);
        }

        public void Info(string message, params object[] parameters)
        {
            Reconfig();
            _logger.Info(String.Format(message, parameters));
        }

        public void Trace(string message)
        {
            throw new NotSupportedException();
        }

        public void Warn(string message)
        {
            Reconfig();
            _logger.Warn(message);
        }

        public void Error(string message)
        {
            Reconfig();
            _logger.Error(message);
        }

        public void Error(string message, Exception exception)
        {
            Reconfig();
            _logger.Error(message, exception);
        }

        public void Error(Exception exception)
        {
            Reconfig();
            _logger.Error(exception);
        }

        public void Fatal(string message)
        {
            Reconfig();
            _logger.Fatal(message);
        }

        public void Fatal(string message, Exception exception)
        {
            Reconfig();
            _logger.Fatal(message, exception);
        }

        public void Fatal(Exception exception)
        {
            Reconfig();
            _logger.Fatal(exception);
        }

        private static DateTime _lastDate;
        private static void Reconfig()
        {
            if (DateTime.Now.Date != _lastDate.Date)
            {
                log4net.Config.XmlConfigurator.Configure();
                _lastDate = DateTime.Now;
            }
        }
    }
}