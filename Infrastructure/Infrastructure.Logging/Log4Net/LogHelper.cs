﻿namespace Infrastructure.Logging.Log4Net
{
    using System.Collections.Generic;
    using System.Text;

    public class LogHelper
    {
        public static string GetProperties<T>(T ob)
        {
            var type = typeof(T);
            var properties = type.GetProperties();
            var result = new StringBuilder();
            foreach (var propertyInfo in properties)
            {
                result.AppendFormat("{0}: {1}, ", propertyInfo.Name, propertyInfo.GetValue(ob));
            }
            return result.ToString().TrimEnd(new[] { ' ', ',' });
        }

        public static string GetProperties<T>(IEnumerable<T> obs)
        {
            if (obs == null)
            {
                return string.Empty;
            }

            var type = typeof(T);
            var properties = type.GetProperties();
            var result = new StringBuilder();
            foreach (var ob in obs)
            {
                result.Append("{");
                foreach (var propertyInfo in properties)
                {
                    result.AppendFormat("{0}: {1}, ", propertyInfo.Name, propertyInfo.GetValue(ob));
                }
                result.Append("}");
            }
            return result.ToString();
        }

        public static string GetProperties<T>(List<T> list)
        {
            return GetProperties((IEnumerable<T>)list);
        }
    }
}