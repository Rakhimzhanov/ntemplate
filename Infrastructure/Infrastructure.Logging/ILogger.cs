﻿namespace Infrastructure.Logging
{
    using System;

    public interface ILogger
    {
        void Debug(string message);
        void Info(string message);
        void Info(string message, params object[] parameters);
        void Trace(string message);
        void Warn(string message);
        void Error(string message);
        void Error(string message, Exception exception);
        void Error(Exception exception);
        void Fatal(string message);
    }
}